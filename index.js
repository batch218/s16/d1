// console.log("hello world")

// [SECTION] Assignment Operators
	// Basic Assignment Operator (=)
	// The assignment operators assign the value of the right hand oprerand to a variable 
	let assignmentNumber = 8;
	console.log("The value of the assignment variable : ", + assignmentNumber)
console.log("=========================");

// [SECTION] Arithmetic Operations

let x = 200 ;
let y = 18 ;

console.log("x = "+x);
console.log("x = "+y);
console.log("=========================");
// Addition

let sum = x + y;
console.log("Result of addition operator:"+sum);
// Subtraction
let subtraction = x - y;
console.log("Result of subtraction operator:"+subtraction);
// Multiplication
let multiply = x * y;
console.log("Result of multiply operator:"+multiply);

// Division
let divide = x / y;
console.log("Result of division operator:"+divide);
// modulo for remainder
let modulo = x % y ;
console.log("Result of modulo operator:"+ modulo);

// Continuation of assignment operator

// AdditionAssignment Operator
	// Current value of assignmentNumber is 8
	// long method
	 // assignmentNumber = assignmentNumber + 2; 
	 // console.log(assignmentNumber)

// Short Hand Method
// addition 
assignmentNumber += 2 ;
console.log("Result of addition Assignment operator" + assignmentNumber);

// Subtraction
assignmentNumber -= 3 ;
console.log("Result of subtraction Assiment operator" + assignmentNumber);

// Multiplication
assignmentNumber *= 3 ;
console.log("Result of multiply Assignment operator" + assignmentNumber);

// Division
assignmentNumber /= 3 ;
console.log("Result of division Assignment operator" + assignmentNumber);

// Modulus
assignmentNumber %= 3 ;
console.log("Result of Modulus Assignment operator " + assignmentNumber);

// Exponential
assignmentNumber **= 3 ;
console.log("Result of Exponential operator:" + assignmentNumber);

// [SECTION] PEMDAS (order of Operations)
// Multiple Operators and Parehthesis

let mdas = 1 + 2- 3 * 4 / 5 ;
console.log("Result of mdas operation:"+ mdas);
/*
	The  operations were done in the following order:
	1. 3 * 4 = 12 | 1+2 - 12 /5 
	2. 12 / 5 * 2.4 | 1+2 - 2.4
	3/ 1 + 2 - 3 | 3 - 2.4 
	4. 3 - 2.4 = 0.6
*/


let pemdas = (1 + (2-3)) * (4/5);
console.log("Result of pemdas operation:" + pemdas);
// heirarchy
// combinations of multiple arithmetic operators will follow the pemdas rule
/*
	1. Parenthesis
	2.exponent
	3. Multiplcation or division
	4. Addition or subtraction

	Note: will also follow left to right rule
*/
// [SECTION] Increment and Decrement
// Increment - Increasing
// Decrement - Decresing
// Operators that or substract values by 1 and reassign the value of the variable where the increment(++) | decrement(--) was applied.
console.log("=========================");
let z = 1;

// pre-increment
let increment = ++z;
console.log("Result of pre increment:" + increment);
console.log("Result of pre increment of z:" + z);

// post-increment
increment1 = z++;
console.log("Result of post increment:" + increment1);
console.log("Result of post increment of z:" + z);

console.log("=========================");

// pre-decrement
let decrement = --z;
console.log("Result of pre decrement:" + decrement); //2
console.log("Result of pre decrement of z:" + z); //2

// post decrement
decrement = z--;
console.log("Result of post decrement:" + decrement);//2
console.log("Result of post decrement of z:" + z);//1
console.log("=========================");

// [SECTION] Type Coercion
// is the automatic conversion of values from one data type to another
// number and string will result to string
let numA = 10;
let numB = "12";

let coercion = numA + numB ;
console.log(coercion);
console.log(typeof coercion);
console.log("=========================");
let numC = 16;
let numD = 14;
nonCoercion = numC + numD ;
console.log(nonCoercion);
console.log(typeof nonCoercion);
console.log("=========================");

//number and boolean data type will result to number 
let numX =  10;
let numY = true ;
			// boolean values are 
			// true = 1
			//  false = 0
coercion = numX + numY ; 
console.log(coercion);
console.log(typeof coercion);
console.log("=========================");

let num1 = 1 ;
let num2 = false ;

coercion = num1 + num2 ;
console.log(coercion);
console.log(typeof coercion);
console.log("=========================");

// [SECTION] Comparison Operators
// Comparison operators are used to evaluate and compare the left and right operands
// After evaluation, it returns a boolean value

// Equality Operator
// Compares the value, but not the data type
console.log(1 == 1); //true
console.log(1 == 2); // false
console.log(1 == '1') //true
console.log(0 == false); //true // flase boolean value is also equal to zero
console.log("=========================");
let juan = "juan";
console.log('juan' == 'juan');// true
console.log('juan' == 'Juan');// false //equality is strict with letter casing
console.log(juan == "juan"); // true
console.log("=========================");

// inequality operator (!=)
console.log(1 != 1); //false
console.log(1 != 2); // true
console.log(1 !='1') //false
console.log(0 != false); //false
console.log("=========================");

console.log('juan' !=  'juan');// false
console.log('juan' !=  'Juan');// true //equality is strict with letter casing
console.log(juan !=  "juan"); // false
console.log("=========================");

// [SECTION] Relational Operators
// Some comparison operators to check wether one value is greater or less than the other value

let a = 50 ;
let b = 65 ;
// GT or Greater Than Operator (>)
let isGreaterThan = a > b ; //50 > 65 = false
console.log(isGreaterThan);
console.log("=========================");
// LT or Less Than Operator (>)
let isLessThan = a < b ; //50 < 65 = true
console.log(isLessThan);
console.log("=========================");
// GT or Equal or Greater Than or Equal Operator (>=)
let isGTEhan = a >= b ; //50 < 65 = false
console.log(isGTEhan);

// forced coercion
let e = 56;
let numStr = "30";
console.log(e > numStr); //true
// forced coercion to change string to number

let str = "twenty";
console.log(e >= str);
// Since the string is not numeric, the string will not be converted to a number, or a.k.a NaN (Not a Number);

console.log("=========================");

// Less Than or Equal or Less Than or Equal Operator (<=)
let isLTEThan = a <= b ; //50 < 65 = true
console.log(isLTEThan);
console.log("=========================");

//Logical AND operator



let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator:" + allRequirementsMet);
// And Operator requires all/ both are true

// Logical OR operator (||)
let someRequirmentsMet = isLegalAge || isRegistered ; //true
console.log("Result of logical OR Operator:" + someRequirmentsMet);
// OR operator requires only 1 True;

// Logical Not Operator (!)
let someRequirmentNotsMet = !isLegalAge; //false
console.log("Result of logical NOT Operator:" + someRequirmentNotsMet);

